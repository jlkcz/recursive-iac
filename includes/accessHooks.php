<?php

/** Function: efIACAccessControlEditFilterHook( &$editor, $text, $section, 
 *            &$error )
 * Hook: EditFilter
 * Intercept saving and previewing a page to protect from:
 *  - Transclusion of protected pages
 *  - Locking editor out of page accidentally
 * &$editor --> (editable) Editor object being used. Member variable 
 *               textbox1 is what should be changed if needed.
 * $text --> Text of the change. This is a copy of $editor->textbox1
 * $section --> Section being edited (not used)
 * &$error --> (editable) Error to return. If anything is in this variable, 
 *               page will not save.
 * Returns: Whether to continue processing or deny access. This extension 
 *           always returns true.
 *	      Even if true, if $error has text save will be stopped.
 * i18n variables: locking-self-out, protected-transclusion
 * NOTE: Preview protection requires a change in the MediaWiki source
 */
function efIACAccessControlEditFilterHook( $editor, $text, $section, 
					   &$error ){		

  //Support for MW 1.16, as on preview it performs this hook as well
  global $wgRequest;

  if(!$wgRequest->getCheck('wpSave')){
      //if this is only preview, end it. We don't care, there is another hook
      return True;
  }

  efIACDebugLog( "(efIACAccessControlEditFilterHook) checking edit..." );
  
	// Display an error if this will lock the person saving the page 
        // out from reading
  if( efIACUserLockingThemselvesOut( $editor->textbox1 ) ){
    $error = efIACGetMessage( 'locking-self-out' );
  }
	
  // If we don't care about transclusions, let it through without 
  // further processing
  global $egBlockRestrictedTransclusions;
  if( !$egBlockRestrictedTransclusions ){
    return true;
  }

  $found = efIACGetTransclusionsFromContent($text);
  $num_matches = $found["num"];

  // Let it through if there are no transclusions
  if( $num_matches == 0 ){
    return true;
  }

  //get accessgroups for edited page
  $accessArray = efIACGetAccessArrayFromTitle($editor->mTitle->mTextform,$editor->mTitle->mNamespace);
  $editedAccessGroups = explode(",,",efIACCheckOverride($accessArray["accesslist"]));

  $protectedArray = Array();
  // For each transclusion, look for an access control list
  for( $i = 0; $i < $num_matches; $i++ ){
    // Get the title being transcluded
      $parts = explode("|",$found["matches"][1][$i]);
      $link_title = $parts[0];

      $transcludeAccessList = efIACGetAccessArrayFromTitle($link_title);
      $transcludeAccessList["accesslist"] = efIACCheckOverride($transcludeAccessList["accesslist"]);

    // If there's an access list, check, if every group, that has access to edited page has also access to transcluded page
    if( $transcludeAccessList !== false and $transcludeAccessList["accesslist"] !== False){

        $transcludeAccessGroups = explode(",,",$transcludeAccessList["accesslist"]);

        foreach($editedAccessGroups as $group){
            //For every group with allowed access to edited page there must be group in transcluded page (so they are same or transcluded is bigger)
            if(!in_array($group,$transcludeAccessGroups)){
                //add this title to guys who should be protected
                $protectedArray[] = $link_title;
                break;
            }
        }
    }
  }

  // Replace the protected transclusions with other text
  foreach($protectedArray as $protect){
    $protectedInfo = efIACGetMessage( 'protected-transclusion',
					 $protect );

      efIACDebugLog( "(efIACAccessControlEditFilterHook) ".
          "blocked transclusion to ".$protect);

      $text = str_replace( '{{:'.$protect.'}}', '<nowiki>{{:'.$protect.'}}</nowiki>'.$protectedInfo , $text );
    
    // Set the new text back into the editor object
    $editor->textbox1 = $text;
  }
  unset($transcludeAccessList);
  unset($transcludeAccessGroups);
  unset($protectedInfo);
  unset($protectedArray);
  unset($editedTitle);
  unset($editedAccessGroups);
  unset($editedAccessArray);
  return true;
}

/** function efIACAccessControlPreviewFilterHook($editPage, &$toparse)
 * $editpage - EditPage object
 * $toparse - submitted text for preview
 * returns True - but it doesn't metter
 * This function is very similar to efIACAccessControlEditFilterHook but I am leaving it this way intentionally
 */
function efIACAccessControlPreviewFilterHook($editPage, &$toparse){
    //This causes memory exhausted error on MW 1.16
    //$editedTitle =$editPage->getTitle();
    $editedTitle = $editPage->mTitle;
    //check if we are previewing nonexistent page
    if(!$editedTitle->exists()){
        $editedAccessArray = False;
    }
    else{
        //get accessgroups for edited page
        $editedAccessArray = efIACGetAccessArrayFromTitle($editedTitle->mTextform,$editedTitle->mNamespace);
    }

    if ($editedAccessArray !== False and $editedAccessArray["accesslist"] !== False ){
        $editedAccessGroups = explode(",,",efIACCheckOverride($editedAccessArray["accesslist"]));
    }
    else{
        $editedAccessGroups = Array();
    }


    // If we don't care about transclusions, let it through without
    // further processing
    global $egBlockRestrictedTransclusions;
    if( !$egBlockRestrictedTransclusions ){
        return true;
    }

    $found = efIACGetTransclusionsFromContent($toparse);
    $num_matches = $found["num"];

    // Let it through if there are no transclusions
    if( $num_matches == 0 ){
        return true;
    }

    //array of transclusions to be protected
    $protectedArray = Array();

    for( $i = 0; $i < $num_matches; $i++ ){
        // Get the title of page being transcluded
        $parts = explode("|",$found["matches"][1][$i]);
        $match = $parts[0];

        //get access data for transcluded page
        $transcludeAccessList = efIACGetAccessArrayFromTitle($match);
        $transcludeAccessList["accesslist"] = efIACCheckOverride($transcludeAccessList["accesslist"]);

        //if transcluded page hase accesscontrol and edited page does not
        if($transcludeAccessList["accesslist"] !== False and empty($editedAccessGroups)){
            $protectedArray[] = $match;
            continue;
        }


        // If there's an access list, check, if every group, that has access to edited page has also access to transcluded page
        if( $transcludeAccessList !== false and $transcludeAccessList["accesslist"] !== False){
            $transcludeAccessGroups = explode(",,",$transcludeAccessList["accesslist"]);

            foreach($editedAccessGroups as $group){
                //For every group with allowed access to edited page there must be group in transcluded page (so they are same or transcluded is bigger)
                if(!in_array($group,$transcludeAccessGroups)){
                    //add this title to guys who should be protected
                    $protectedArray[] = $match;
                    break;
                }
            }
        }
    }

    $protectedInfo = efIACGetMessage( 'protected-preview-transclusion',
        Array() );
    foreach($protectedArray as $protect){
        efIACDebugLog( "(efIACAccessControlPreviewFilterHook) ".
            "blocked preview transclusion to ".$protect);

        $toparse = str_replace( '{{:'.$protect.'}}', '<nowiki>{{:'.$protect.'}} -- '.$protectedInfo.'</nowiki>' , $toparse );


    }
    unset($transcludeAccessList);
    unset($transcludeAccessGroups);
    unset($protectedInfo);
    unset($protectedArray);
    unset($editedTitle);
    unset($editedAccessGroups);
    unset($editedAccessArray);
    return True;
}

/** Function efIACGetTransclusionsFromContent
 * $content - string
 * return Array(
 *  "num" => number of matches
 *  "matches" => array of matches as returned from preg_match_all
 */
function efIACGetTransclusionsFromContent($content){
    // Set the expression for a transclusion

    //we don't need to protect <nowiki> transclusions
    $content = preg_replace('/<nowiki[^>]*>.*?<\/nowiki>/i', '', $content);


    $regexp = '{{:(.*?)}}';
    // Search for transclusions in the edit
    $num_matches = preg_match_all( $regexp, $content, $matches );

    return Array(
        "num" => $num_matches,
        "matches" => $matches
    );
}

/** Function: efIACAccessControlFetchChangesHook( $user, $skin, &$list )
 * Hook: FetchChangesList
 * Intercept returning the list of Recent Changes and strip out ones user 
 *  has no access to read 
 * $user --> Current user
 * $skin --> Skin being used by user (not used)
 * &$list --> List of recent changes (editable)
 * Returns: Whether to continue with default list (this extension always 
 *		returns false, to replace)
 * NOTE: AccessControlChangesList is a custom class that performs the 
 *         protection
 */
function efIACAccessControlFetchChangesHook( $user, $skin, &$list ){
  $list = new AccessControlChangesList( $skin );
  return false;
}



/** function efIACCheckOverride($accessList)
 * Checks if ALL accesscontrol is present --> then everybody has access to this page
 */
function efIACCheckOverride($accessList) {
    if($accessList === "ALL"){
        return False;
    }
    return $accessList;
}

/** Function: efIACAccessControlTag( $input, $argv, $parser )
 * Prints out <accesscontrol> tag as
 * $input --> Contents (foo) in <accesscontrol>foo</accesscontrol>
 * $definer -> subpage defining accesscontrol
 * Returns:  Message that this page is protected
 * i18n variables: protected-page 
 */
function efIACAccessControlTag($groups, $url, $title ) {
  $identifierCode = '<!-- ACCESSCONTROL PROTECTED PAGES -->';
  return $identifierCode.efIACGetMessage( 'protected-page', Array($groups, $url, $title) );
}

/** Function: efIACAccessControlTagVoid( $input, $argv, $parser )
 * Hook: Parser tag
 * Register the <accesscontrol> tag as and replace it with void
 * $input --> Contents (foo) in <accesscontrol>foo</accesscontrol>
 * $argv --> Tag arguments (not used)
 * $parser --> Parent parser (not used)
 * Returns:  empty string
 * i18n variables: protected-page
 */

function efIACAccessControlTagVoid($input, $argv=null, $parser=null ){
    return "";
}


/** Function: efIACAccessControlUserCanHook( $title, $wgUser, $action,
 *						&$result )
 * Hook: userCan
 * Check the current user's rights to perform an action on a page
 * $title --> Title object for article being accessed
 * $wgUser --> Current user
 * $action --> Action being attempted
 * &$result --> Result to return (modifiable). 
 * Returns: Whether this user has access to the page for this action
 * NOTE: Return value determines whether later functions should be run to 
 *		check access
 *	   $result determines whether this function thinks the user should 
 *		have access
 *	   This extension always returns the same value as $result
 */
function efIACAccessControlUserCanHook( $title, $wgUser, $action, 
					&$result ){
  // Option for whether to pass through if sysop
  global $egAdminCanReadAll;


  // Make sure we're dealing with a Title object
  $title = efIACMakeTitle( $title, $title->getNamespace());

  efIACDebugLog( "(efIACAccessControlUserCanHook) checking access for ".
		 $wgUser->getName()." on '".$title->getText()."'" );
  
  // Check if the user is a sysop
  $userIsSysop = efIACUserIsSysop( $wgUser );
  
  // Pass through if user is a sysop and the option is set
  if( $egAdminCanReadAll && $userIsSysop ){
    efIACDebugLog( "(efIACAccessControlUserCanHook) sysop access");
    return efIACReturnResult( true, $result );
  }
  
  // Fail if article requires sysop and user is not one
  if( efIACArticleRequiresAdmin( $title ) && !( $userIsSysop ) ){
    efIACDebugLog( "(efIACAccessControlUserCanHook) sysop required");
    return efIACReturnResult( false, $result );
  }


  //get access array from our current title
  $tempAccessList = efIACGetAccessArrayFromTitle($title->getText(),$title->getNamespace());

  $accessList = efIACCheckOverride($tempAccessList["accesslist"]);

    // Get the result of whether the user can access
  $localResult = efIACUserCanAccess( $wgUser, $accessList, $action );

  unset($accessList);
  unset($content);
  unset($title);
  
  return efIACReturnResult( $localResult, $result );

}
/*
 * Function efIACGetAccessArrayFromTitle($titleText)
 * $titleText - Text (not object) of title of page we want our access array
 * returns False or Array
 *      "accesslist" - string, content of nearest <accesscontrol> Tag
 *      "definer" - Title object of page giving us accesslist
 * return False when no appropriate <accesscontrol> found
 */
function efIACGetAccessArrayFromTitle($titleText,$namespace=0){
    $currAccessList = False;

    $currTitle = efIACMakeTitle($titleText,$namespace);
    //this loop will go through all parent pages of current page, searching for
    while($currAccessList === False){
        // Get the content of the article
        $content = efIACGetArticleContent( $currTitle,$namespace );

        // Get the access control list from that content
        $currAccessList = efIACGetAccessList( $content );

        //Load new title in case there is no result
        //In case namespace is "Discussion, proceed to regular page"
        if($namespace == 1){
            //other pages will ignore namespace
            $namespace = 0;
            $currTitle = efIACMakeTitle($titleText);
            //Skip the rest, because control if pages are same will stop the "recursion"
            continue;
        }

        //load parent page
        $newTitleText = $currTitle->getBaseText();

        //if new title is same as the old one, there are no more parent pages, just namespace
        if ($newTitleText == $currTitle->getText()){
            //we are at the end but what if our page is in namespace

            //at first check, if we have something from last level
            if($currAccessList === False){
                //classical namespaces are plain broken, let's ignore them and use the best!
                //Fake namespaces FTW!!
                $parts = explode(":",$currTitle->getBaseText());
                if(count($parts) == 1){
                    //There is no fake-namespace
                    break;
                }
                $currTitle = efIACMakeTitle( $parts[0] );
                $nsContent = efIACGetArticleContent( $currTitle );
                $currAccessList = efIACGetAccessList($nsContent);
            }
            //everything was done!
            break;
        }

        //change currTitle for next round (only if we have nothing)
        if($currAccessList === False){
            $currTitle = efIACMakeTitle($newTitleText, $namespace);
        }

    }
    return Array(
        "accesslist" => $currAccessList,
        "definer" => $currTitle
        );
}

/*function efIACAddAccesscontrolTagToPage (&$article, &$text)
 * $article -> some internal bullshit
 * $text -> text of HTML of rendered page
 * returns always true
 * Side-effect: unless there are conditions denying it, it will append accesscontrol line to page
*/
function efIACAddAccesscontrolTagToPage (&$article, &$text) {
    global $wgTitle, $wgScript, $wgRequest ;
    //user is editing
    if ($wgRequest->getText( 'action' )=='edit'){
        return true;
    }
    //user is viewing history
    if ($wgRequest->getText( 'action' )=='history'){
        return true;
    }
    //user is previewing
    if ($wgRequest->getText( 'action' )=='submit'){
        return true;
    }
    //it is special page
    if ( strpos ( $wgTitle->getPrefixedText(), 'Special:' ) === 0 ) return true;
    if ( strpos ( $wgTitle->getPrefixedText(), 'Speciální:' ) === 0 ) return true;

    //get my groups
    $accesscontrolGroups = efIACGetAccessArrayFromTitle($wgTitle->getText(),$wgTitle->getNamespace());



    //if the accesslist is empty or set to override, we don't want to show the lines
    if($accesscontrolGroups === False or $accesscontrolGroups["accesslist"] == "ALL" or $accesscontrolGroups["accesslist"] === False){
        return True;
    }
    //generate line
    $my_message = efIACAccessControlTag($accesscontrolGroups["accesslist"],$accesscontrolGroups["definer"]->getFullUrl(),$accesscontrolGroups["definer"]->getText());

    //append line to text.
    //HERE IS UNEXPECTED SIDE EFFECT!
    $text = $my_message . $text;

    return true;

}


?>
