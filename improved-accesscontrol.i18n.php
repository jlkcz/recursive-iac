<?php

global $egPreventSaveDenyingAccess;

$messages = array();
$messages['cs'] = 
 array(
       'extension-desc' => 
         'Enables group-based access control on a page-by-page basis.',
       'protected-page' => '<div id="accesscontrol" style="color:#BA0000;font-size:8pt;line-height:10pt;background-color: rgbA(255,255,255,0.5);float:right; border: 1px dotted black; padding:0.1em; margin:0.25em;">K této stránce mají přístup lidé z následujících skupin: <b style=" color:black"> $1 </b>&nbsp;!!!<br/>Určeno stránkou <a href="$2">$3</a> <a href="/index.php?title=Accesscontrol">(?)</a><br/></div>',

       'protected-transclusion' => 
         '<!-- Tuhle stránku nejde vložit kvůli accesscontrolu. Co s tím? Vkládaná stránka musí mít stejný nebo volnější (přístup má víc skupin) accesscontrol než tahle. -->',
       'locking-self-out' => 
         '<b>You cannot save content that you will be unable to '.
          $egPreventSaveDenyingAccess.'.',
        'protected-preview-transclusion' => "Tady má být vložená stránka, ale zdá se, že na tohle nemáš práva. Po uložení to možná bude jinak."
    );


$messages['en'] = 
 array(
       'extension-desc' => 
         'Enables group-based access control on a page-by-page basis.',
       'protected-page' => '<div id="accesscontrol" style="color:#BA0000;font-size:8pt;line-height:10pt;background-color: rgbA(255,255,255,0.5);float:right; border: 1px dotted black; padding:0.1em; margin:0.25em;">K této stránce mají přístup lidé z následujících skupin: <b style=" color:black"> $1 </b>&nbsp;!!!<br/>Určeno stránkou <a href="$2">$3</a> <a href="/index.php?title=Accesscontrol">(?)</a><br/></div>',

       'protected-transclusion' => 
         '<!-- Tuhle stránku nejde vložit kvůli accesscontrolu. Co s tím? Vkládaná stránka musí mít stejný nebo volnější (přístup má víc skupin) accesscontrol než tahle. -->',
       'locking-self-out' => 
         '<b>You cannot save content that you will be unable to '.
          $egPreventSaveDenyingAccess.'.',
        'protected-preview-transclusion' => "Tady má být vložená stránka, ale zdá se, že na tohle nemáš práva. Po uložení to možná bude jinak."
    );


?>
